var express = require('express');
var _ = require('lodash');
var router = express.Router();


var todos = [];

router.get('/', function(req, res, next) {
  res.json(todos);
});

router.get('/:id', function(req, res, next) {

  var todo = _.find(todos, {id: req.params.id});
  if (todo) {
    res.json(todo);
  } else {
    res.send(404);
  }

});

router.put('/:id/_markascomplete', function(req, res, next) {

  var todo = _.find(todos, {id: req.params.id});
  if (todo) {
    todo.complete = true;
    res.json(todo);
  } else {
    res.send(404);
  }

});

router.delete('/:id', function(req, res, next) {
  _.remove(todos, {id: req.params.id})
  res.send(204);
});

router.post('/', function(req, res, next) {
  var todo = req.body;

  if (!todo.id) {
    todo.id = _.uniqueId();
  }

  todos.push(todo);

  res.json(todo);
});

module.exports = router;
